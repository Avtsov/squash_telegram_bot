var fs = require('fs');
var config = require('./../config');
var log = require('./../general_methods/log');

module.exports = function (token) {
    try {
        fs.mkdirSync(config.google_sheets.TOKEN_DIR);
    } catch (err) {
        if (err.code !== 'EEXIST') {
            throw err;
        }
    }
    fs.writeFile(config.google_sheets.TOKEN_PATH, JSON.stringify(token));
    log('Token stored to ' + config.google_sheets.TOKEN_PATH);
};
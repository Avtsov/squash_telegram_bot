var config = require('./../config');
var readline = require('readline');
var storeToken = require('./storeToken');
var log = require('./../general_methods/log');

module.exports = function (oauth2Client, callback) {
    var authUrl = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: config.google_sheets.SCOPES
    });

    var authBotToken = '432470395:AAG_msE_hEqn-zv89DNUT1FniBGJ36uyFss';
    var TelegramBot = require('node-telegram-bot-api');
    var bot = new TelegramBot(authBotToken, {polling: true});
    bot.sendMessage(76646126,'Authorize this app by visiting this url: '+authUrl);
    bot.on('message',function (msg) {
        var code = msg.text;
        oauth2Client.getToken(code, function(err, token) {
            if (err) {
                log('Error while trying to retrieve access token', err);
                return;
            }
            oauth2Client.credentials = token;
            storeToken(token);
            callback(oauth2Client);
        });
    });
};
var google = require('googleapis');
var telegram_bot = require('./../telegram_bot_methods/telegram_bot');
var fs = require('fs');
var authorize = require('./authorize');
var log = require('./../general_methods/log');
var config = require('./../config');
var moment = require('moment');

module.exports = function (auth,callback) {
    var sheets = google.sheets('v4');
    sheets.spreadsheets.values.get({
        auth: auth,
        spreadsheetId: config.spreadsheetId,
        range: 'A1:D'
    }, function(err, response) {
        var ajaxGoogleSheet = require('./ajaxGoogleSheet');
        if (err) {
            log('The Google Sheets API returned an error on get: ' + err);
            return;
        }
        log('Sheets data updated',callback);
        global.response = {
            nickScore: response.values[2][2],
            temiiScore: response.values[2][3],
            results: response.values.slice(3)
        };
        global.lastUpdated = moment().format('MMMM Do YYYY, h:mm:ss a');
        if (!global.botCreated) {
            global.botCreated = telegram_bot();
        }
        setTimeout(function () {
            ajaxGoogleSheet('get')
        },3600000)
    });
}
;
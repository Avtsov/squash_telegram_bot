var fs = require('fs');
var authorize = require('./authorize');
var getGoogleSheetData = require('./getGoogleSheetData');
var setGoogleSheetData = require('./setGoogleSheetData');
var log = require('./../general_methods/log');

module.exports = function (type,callback,options) {
    var action = type === 'get' ? getGoogleSheetData : setGoogleSheetData;
    fs.readFile('client_secret.json', function processClientSecrets(err, content) {
        if (err) {
            log('Error loading client secret file: ' + err);
            return;
        }
        authorize(JSON.parse(content), function (auth) {
            action(auth,callback,options);
        });
    });
};
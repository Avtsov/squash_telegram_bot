var google = require('googleapis');
var fs = require('fs');
var authorize = require('./authorize');
var log = require('./../general_methods/log');
var config = require('./../config');

module.exports = function (auth,callback,options) {
    var ajaxGoogleSheet = require('./ajaxGoogleSheet');
    var sheets = google.sheets('v4');
    sheets.spreadsheets.values.update({
        auth: auth,
        spreadsheetId: config.spreadsheetId,
        range: options.range,
        valueInputOption: 'USER_ENTERED',
        resource: {
            values: options.values
        }
    },function (err) {
        if (err) {
            log('The Google Sheets API returned an error on save: ' + err);
            return;
        }
        log('Sheets data saved',callback);
        ajaxGoogleSheet('get');
    })
};
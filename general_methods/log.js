var google = require('googleapis');
var fs = require('fs');
var moment = require('moment');
var config = require('./../config');

module.exports = function (logMessage,callback) {
    var authorize = require('./../google_sheets_methods/authorize');
    fs.readFile('client_secret.json', function processClientSecrets(err, content) {
        if (err) {
            log('Error loading client secret file: ' + err);
            return;
        }
        authorize(JSON.parse(content), function (auth) {
            var sheets = google.sheets('v4');
            sheets.spreadsheets.values.append({
                auth: auth,
                spreadsheetId: config.google_sheets.logSpreadSheetId,
                valueInputOption: 'USER_ENTERED',
                range:'A:B',
                resource: {
                    values: [
                        [moment().format('MMMM Do YYYY, h:mm:ss a')+':',logMessage]
                    ]
                }
            },function (err) {
                if (err) {
                    console.log('The Google Sheets API returned an error on log: ' + err);
                    return;
                }
                !!callback && callback();
            })
        });
    });
};
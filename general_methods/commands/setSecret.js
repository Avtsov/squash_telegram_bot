var isAdmin = require('./isAdmin'),
    config = require('./../../config'),
    log = require('./../log');

module.exports = function (match) {
    var newSecret = match.input.split(' ')[1],
        returnMessage = 'You\'re not an authorized person, sorry.';
    if (!newSecret) return 'Missing secret key to set.';
    if (isAdmin()) {
        returnMessage = 'The secret is successfully set to: '+newSecret+'.';
        config.secret = newSecret;
        log(returnMessage);
        return returnMessage;
    } else {
        log(returnMessage);
        return returnMessage;
    }
};
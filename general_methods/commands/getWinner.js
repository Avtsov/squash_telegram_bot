var defaults = require('./../../defaults');

module.exports = function (name) {
    var res = global.response;
    if (res.nickScore > res.temiiScore) {
        return !!name ? 'Nick' : {
            image: defaults.images.nick,
            name: 'Nick is winning.'
        }
    } else if (res.nickScore < res.temiiScore) {
        return !!name ? 'Temii' : {
            image: defaults.images.temii,
            name: 'Temii is winning.'
        }
    } else return !!name ? 'Tie' : {
        image: defaults.images.both,
        name: 'It is tie!'
    }
};
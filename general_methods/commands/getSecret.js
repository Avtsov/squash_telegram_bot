var isAdmin = require('./isAdmin'),
    config = require('./../../config');

module.exports = function () {
    if (isAdmin()) return 'The secret is '+config.secret;
    else return 'You\'re not an authorized person, sorry.'
};
var config = require('./../../config');

module.exports = function (un) {
    return config.admins.indexOf(un)!==-1 ? 'Yes, you\'re an admin.' : 'No, you\'re not an admin, sorry.';
};
var isValidSecret = require('./../isValidSecret');
var config = require('./../../config');

module.exports = function (bot,msg,match) {
    var ajaxGoogleSheet = require('./../../google_sheets_methods/ajaxGoogleSheet');
    match = match.input.split(' ');
    var fromId = msg.from.id,
        un = msg.from.username,
        options = {
            date: '',
            temiiScore: '',
            nickScore: '',
            secret: ''
        },
        missingRequiredOptions = [];

    match.map(function (val) {
        var flag = val.slice(0,2);
        if (flag==='D:' || flag==='d:') {
            options.date = val.slice(2);
        } else if (flag==='T:' || flag==='t:') {
            options.temiiScore = val.slice(2);
        } else if (flag==='N:' || flag==='n:') {
            options.nickScore = val.slice(2);
        } else if (flag==='S:' || flag==='s:') {
            options.secret = val.slice(2);
        }
    });

    Object.keys(options).filter(function (val) {
        if (val!=='secret' && !options[val]) {
            missingRequiredOptions.push(val);
        } else if (val==='secret') return;
        return !!val;
    });

    !missingRequiredOptions.length ? (function () {
        if (config.admins.indexOf(un)!==-1 || isValidSecret(options.secret)) {
            ajaxGoogleSheet(
                'set',
                function () {
                    bot.sendMessage(fromId,'Data successfully saved.');
                },
                {
                    range: !!options.range ? options.range : 'A'+(global.response.results.length+4)+':D'+(global.response.results.length+4),
                    values: [[global.response.results.length+1,options.date,options.nickScore,options.temiiScore]]
                }
            );
        } else {
            bot.sendMessage(fromId,'You\'re not authorized to set scores, sorry.');
        }
    })() : bot.sendMessage(fromId,'Missing required options: '+missingRequiredOptions.join(', '));
};
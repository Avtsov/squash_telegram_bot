module.exports = function () {
    var res = global.response,
        gamesPlayedNum = (parseInt(res.nickScore)+parseInt(res.temiiScore));
    return 'You\'ve played '+gamesPlayedNum+' games so far.';
};
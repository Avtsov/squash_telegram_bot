module.exports = function () {
	var res = global.response;

	var draws = 0;
	var temiiWon = 0;
	var nickWon = 0;
	var maxDif = 0;
	var gameId = 0;

	for (var i = res.results.length - 1; i >= 0; i--) {
		var dif = res.results[i][2] - res.results[i][3];
		if (Math.abs(dif) > maxDif) {
			maxDif = dif;
			gameId = i;
		}
	}

	for (var i = res.results.length - 1; i >= 0; i--) {
		var nickScore  = parseInt(res.results[i][2]);
		var temiiScore = parseInt(res.results[i][3]);

		if (nickScore === temiiScore) {
			draws++;
		} else if (nickScore < temiiScore) {
			temiiWon++;
		} else if (nickScore > temiiScore) {
			nickWon++;
		} else {
			return "smth wrong";
		}
	}

	function percentForResult(num) {
		return Math.ceil(100 * num / res.results.length);
	}

	return "Results"  + "  |  "  + "Total" + "  |  "  + "%\n" +
		   "--------------------------------\n" + 
		   "Nick           "  + nickWon  + "          " + percentForResult(nickWon) + "\n" +
		   "Temii         " + temiiWon  + "          " + percentForResult(temiiWon) + "\n" +
		   "Draws        " + draws  + "           " + percentForResult(draws) + 
		   "\n\n" + 
		   "Best win: \n" + 
		   res.results[gameId][1] + ', Nick '+res.results[gameId][2]+' - '+res.results[gameId][3]+' Temii';
};
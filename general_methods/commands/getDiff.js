var getWinner = require('./getWinner');

module.exports = function () {
    var res = global.response,
        diff = res.nickScore>res.temiiScore ? res.nickScore-res.temiiScore : res.temiiScore-res.nickScore;
    return getWinner(true)!=='Tie' ?
        getWinner(true)+' is currently ahead by '+diff+' points.' :
        getWinner(true);
};
module.exports = function (date) {
    var res = global.response;
    if (!!date) {
        var returnVal = '',
            d = Date.parse(date);
        res.results.forEach(function (result) {
            if (Date.parse(result[1])===d) {
                returnVal = 'Nick '+result[2]+' - '+result[3]+' Temii';
            }
        });
        return returnVal;
    } else {
        return 'Nick '+res.nickScore+' - '+res.temiiScore+' Temii';
    }
};
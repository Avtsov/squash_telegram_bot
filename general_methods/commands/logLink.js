var defaults = require('./../../defaults');

module.exports = function () {
    return 'Link to the spreadsheet with logs:\n'+defaults.linkToLogSpreadsheet;
};
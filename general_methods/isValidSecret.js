var config = require('./../config');

module.exports = function (secretToValidate) {
    return secretToValidate === config.secret;
};
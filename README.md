# Squash bot #

[Link to the bot in Telegram](https://t.me/squash_sports_bot)

This is a README for the squash bot. The aim of this bot is to conveniently track the progress of the Squash Tournament 2017' between Artemii and Nick.

### What you can do with this bot? ###

* Get the most up-to-date information about the currently winning player (name and photo)
* Record new scores after each game day
* Get awesome statistics regarding the Tournament (e.g. what is the difference in score ATM)

### What technologies are used in this bot? ###

* Google Spreadsheets (as a DB)
* Node.js
* Moment.js
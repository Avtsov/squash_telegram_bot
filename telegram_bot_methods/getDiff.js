var getDiff = require('./../general_methods/commands/getDiff');

module.exports = function (msg,bot) {
    var fromId = msg.from.id;
    var resp = getDiff();
    bot.sendMessage(fromId, resp);
};
var getWinner = require('./../general_methods/commands/getWinner');

module.exports = function (msg,res,bot) {
    if (typeof msg === 'object') {
        var chatId = msg.chat.id;
        var resp = getWinner();
        bot.sendPhoto(chatId, resp.image, {caption: resp.name});
    } else {
        return getWinner(true);
    }
};
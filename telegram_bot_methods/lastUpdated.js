var lastUpdated = require('./../general_methods/commands/lastUpdated');

module.exports = function (msg,bot) {
    var fromId = msg.from.id;
    var resp = lastUpdated();
    bot.sendMessage(fromId, resp);
};
var logLink = require('./../general_methods/commands/logLink');

module.exports = function (msg,bot) {
    var fromId = msg.from.id;
    var resp = logLink();
    bot.sendMessage(fromId, resp);
};
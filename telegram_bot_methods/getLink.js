var getLink = require('./../general_methods/commands/getLink');

module.exports = function (msg,bot) {
    var fromId = msg.from.id;
    var resp = getLink();
    bot.sendMessage(fromId, resp,{'disable_web_page_preview':true});
};
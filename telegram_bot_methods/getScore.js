var getScore = require('./../general_methods/commands/getScore');

module.exports = function (msg,match,bot) {
    var fromId = msg.from.id;
    var date = match[1];
    var resp = 'The current score is:\n'+getScore(/*date*/);
    bot.sendMessage(fromId, resp);
};
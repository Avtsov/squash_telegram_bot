var refresh = require('./../general_methods/commands/refresh');

module.exports = function (msg,bot) {
    var fromId = msg.from.id;
    refresh(bot,fromId);
};
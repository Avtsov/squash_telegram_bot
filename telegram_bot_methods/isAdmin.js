var isAdmin = require('./../general_methods/commands/isAdmin');

module.exports = function (msg,bot) {
    var fromId = msg.from.id;
    var un = msg.from.username;
    var resp = isAdmin(un);
    bot.sendMessage(fromId, resp);
};
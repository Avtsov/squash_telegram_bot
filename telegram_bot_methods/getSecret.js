var getSecret = require('./../general_methods/commands/getSecret');

module.exports = function (msg,bot) {
    var fromId = msg.from.id;
    var resp = getSecret();
    bot.sendMessage(fromId, resp);
};
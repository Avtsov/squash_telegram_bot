var getStat = require('./../general_methods/commands/getStat');

module.exports = function (msg, bot) {
    bot.sendMessage(msg.from.id, getStat());
};
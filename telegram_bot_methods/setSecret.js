var setSecret = require('./../general_methods/commands/setSecret');

module.exports = function (msg,match,bot) {
    var fromId = msg.from.id;
    var resp = setSecret(match);
    bot.sendMessage(fromId, resp);
};
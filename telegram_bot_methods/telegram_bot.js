var TelegramBot = require('node-telegram-bot-api'),
    config = require('./../config'),
    getLink = require('./../telegram_bot_methods/getLink'),
    getScore = require('./../telegram_bot_methods/getScore'),
    getDiff = require('./../telegram_bot_methods/getDiff'),
    gamesPlayed = require('./../telegram_bot_methods/gamesPlayed'),
    isAdmin = require('./../telegram_bot_methods/isAdmin'),
    getWinner = require('./../telegram_bot_methods/getWinner'),
    refresh = require('./../telegram_bot_methods/refresh'),
    lastUpdated = require('./../telegram_bot_methods/lastUpdated'),
    setScore = require('./../telegram_bot_methods/setScore'),
    getStat = require('./../telegram_bot_methods/getStat'),
    getSecret = require('./../telegram_bot_methods/getSecret'),
    setSecret = require('./../telegram_bot_methods/setSecret'),
    logLink = require('./../telegram_bot_methods/logLink'),
    log = require('./../general_methods/log');

var token = config.telegram.telegramToken;
var res = global.response;

module.exports = function () {
    var bot = new TelegramBot(token, {polling: true});

    bot.onText(/\/link/, function (msg) {
        getLink(msg,bot);
    });
    bot.onText(/\/winner/, function (msg) {
        getWinner(msg,res,bot);
    });
    bot.onText(/\/getscore (.+)|\/getscore/, function (msg,match) {
        getScore(msg,match,bot);
    });
    bot.onText(/\/setscore/, function (msg,match) {
        setScore(msg,match,bot);
    });
    bot.onText(/\/getdiff/, function (msg) {
        getDiff(msg,bot);
    });
    bot.onText(/\/gamesplayed/, function (msg) {
        gamesPlayed(msg,bot);
    });
    bot.onText(/\/isadmin/, function (msg) {
        isAdmin(msg,bot);
    });
    bot.onText(/\/refresh/, function (msg) {
        refresh(msg,bot);
    });
    bot.onText(/\/lastupdated/, function (msg) {
        lastUpdated(msg,bot);
    });
    bot.onText(/\/getstat/, function (msg) {
        getStat(msg, bot);
    });
    bot.onText(/\/getsecret/, function (msg) {
        getSecret(msg, bot);
    });
    bot.onText(/\/setsecret/, function (msg,match) {
        setSecret(msg,match,bot);
    });
    bot.onText(/\/loglink/, function (msg) {
        logLink(msg,bot);
    });

    bot.on('message',function (msg) {
        log('New message from '+msg.chat.username+'. Message: '+msg.text);
    });

    return true;
};
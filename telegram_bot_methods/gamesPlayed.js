var gamesPlayed = require('./../general_methods/commands/gamesPlayed');

module.exports = function (msg,bot) {
    var fromId = msg.from.id;
    var resp = gamesPlayed();
    bot.sendMessage(fromId, resp);
};
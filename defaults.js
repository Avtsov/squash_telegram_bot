var fs = require('fs');
var config = require('./config');

module.exports = {
    spreadsheetId: config.spreadsheetId,
    linkToSpreadsheet: 'https://docs.google.com/spreadsheets/u/0/d/'+config.spreadsheetId+'/edit',
    linkToLogSpreadsheet: 'https://docs.google.com/spreadsheets/u/0/d/'+config.google_sheets.logSpreadSheetId+'/edit',
    images: {
        temii: fs.readFileSync('media/temii.jpg'),
        nick: fs.readFileSync('media/nick.jpg'),
        both: fs.readFileSync('media/both.jpg')
    }
};